﻿using System.Diagnostics;
using System.Management;
using System.Runtime.InteropServices;

namespace net.katrea.trayrunner.tr6;

public delegate void OutputReceived(object sender, string output);

public class Command
{
    private readonly ProcessStartInfo _startInfo;
    private Process? _process;

    public Command(CommandDescriptor commandDescriptor)
    {
        CommandDescriptor = commandDescriptor;
        _startInfo = new ProcessStartInfo
        {
            CreateNoWindow = true,
            UseShellExecute = false,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            RedirectStandardInput = true,
            WindowStyle = ProcessWindowStyle.Hidden,

            FileName = CommandDescriptor.Command,
            Arguments = JoinArguments(CommandDescriptor.CommandArguments),
            WorkingDirectory = CommandDescriptor.WorkingDirectory
        };
    }

    public CommandDescriptor CommandDescriptor { get; }

    public bool Halted { get; private set; }

    public bool Started { get; private set; }

    public event OutputReceived? ErrorReceived;
    public event OutputReceived? OutputReceived;
    public event EventHandler? Terminated;

    [DllImport("kernel32.dll")]
    private static extern bool GenerateConsoleCtrlEvent(uint dwCtrlEvent, uint dwProcessGroupId);

    [DllImport("kernel32.dll")]
    private static extern uint GetProcessId(IntPtr process);

    public void Launch()
    {
        if (Started) throw new InvalidOperationException("Cannot start an already started process");

        Started = true;
        _process = Process.Start(_startInfo) ?? throw new InvalidOperationException("Couldn't start the process");
        Halted = false;

        _process.EnableRaisingEvents = true;
        _process.OutputDataReceived += Process_OutputDataReceived;
        _process.ErrorDataReceived += Process_ErrorDataReceived;
        _process.Exited += Process_Exited;

        Debug.WriteLine("Begin Read Output");
        _process.BeginOutputReadLine();
        Debug.WriteLine("Begin Read Error");
        _process.BeginErrorReadLine();
        _process.StandardInput.Close();
    }

    public void Halt()
    {
        if (!Started || Halted || _process == null)
            return;

        Halted = true;
        Debug.WriteLine("Halting");
        KillProcessAndChildren(_process.Id);
    }

    /// http://stackoverflow.com/questions/23845395/in-c-how-to-kill-a-process-tree-reliably
    /// <param name="pid">Process ID.</param>
    private static void KillProcessAndChildren(int pid)
    {
        var searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + pid);
        var moc = searcher.Get();
        foreach (var o in moc)
        {
            var mo = (ManagementObject)o;
            KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
        }

        try
        {
            var proc = Process.GetProcessById(pid);
            Debug.WriteLine("Killing " + pid);
            proc.Kill();
        }
        catch (ArgumentException)
        {
            // Process already exited. 
        }
    }

    private string JoinArguments(string[] args)
    {
        for (var i = 0; i < args.Length; i += 1)
            if (args[i].IndexOf(' ') > -1)
                args[i] = "\"" + args[i] + "\"";

        return string.Join(" ", args);
    }

    private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
    {
        Debug.WriteLine(e.Data, "OUT");

        OutputReceived?.Invoke(this, e.Data!);
    }

    private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
    {
        Debug.WriteLine(e.Data, "ERROR");

        ErrorReceived?.Invoke(this, e.Data!);
    }

    private void Process_Exited(object? sender, EventArgs e)
    {
        //this.Process.CancelOutputRead();
        //this.Process.CancelErrorRead();
        Halted = true;
        Terminated?.Invoke(this, EventArgs.Empty);
    }
}