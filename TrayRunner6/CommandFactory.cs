namespace net.katrea.trayrunner.tr6;

public class CommandFactory
{
    public static CommandDescriptor CreateCommand(Options options)
    {
        if (options.Command == null) throw new InvalidOperationException("Command shoud be set");

        var optionsCommand = options.Command.ToArray();

        string command;
        var commandArguments = new List<string>();
        if (options.Shell)
        {
            command = Environment.GetEnvironmentVariable("COMSPEC") ?? "cmd.exe";
            commandArguments.Add("/C");
            commandArguments.AddRange(options.Command);
        }
        else
        {
            command = optionsCommand[0];
            commandArguments.AddRange(optionsCommand[1..]);
        }

        return new CommandDescriptor
        {
            Title = options.Title,
            Icon = options.Icon,
            WorkingDirectory = options.WorkingDirectory,
            Command = command,
            CommandArguments = commandArguments.ToArray()
        };
    }
}