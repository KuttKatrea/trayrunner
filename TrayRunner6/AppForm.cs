using System.ComponentModel;
using System.Diagnostics;

namespace net.katrea.trayrunner.tr6;

public sealed class AppForm : Form
{
    private readonly Command _command;
    private readonly TextBox _logBox;
    private readonly ToolStripMenuItem _notifyContextMenuExitItem;

    /// <summary>
    ///     Required designer variable.
    /// </summary>
    private readonly IContainer components;

    private bool _doClose;

    public AppForm(CommandDescriptor commandDescriptor)
    {
        components = new Container();

        SuspendLayout();
        Name = "AppForm";

        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(800, 600);
        AutoScaleDimensions = new SizeF(6F, 13F);
        Text = commandDescriptor.Title;
        if (commandDescriptor.Icon != null) Icon = new Icon(commandDescriptor.Icon);

        var stopButton = new Button
        {
            Text = "Stop",
            Dock = DockStyle.Fill
        };
        var lvColumnType = new ColumnHeader { Text = "Type", Width = 80 };
        var lvColumnMessage = new ColumnHeader { Text = "Message", Width = -2 };
        _logBox = new TextBox
        {
            Name = "logBox",
            Multiline = true,
            Dock = DockStyle.Fill,
            TabIndex = 2,
            ReadOnly = true
        };

        var layout = new TableLayoutPanel
        {
            ColumnCount = 1,
            ColumnStyles = { new ColumnStyle(SizeType.Percent, 50F) },
            Controls =
            {
                { _logBox, 0, 0 },
                { stopButton, 0, 1 }
            },
            Dock = DockStyle.Fill,
            Location = new Point(0, 0),
            Name = "tableLayoutPanel1",
            RowCount = 2,
            RowStyles =
            {
                new RowStyle(SizeType.Percent, 100),
                new RowStyle(SizeType.Absolute, 32F)
            },
            TabIndex = 2
        };

        _notifyContextMenuExitItem = new ToolStripMenuItem
        {
            Name = "notifyContextMenuExitItem",
            Text = "Exit"
        };

        var contextMenu = new ContextMenuStrip(components)
        {
            Name = "notifyContextMenu",
            Items =
            {
                _notifyContextMenuExitItem
            }
        };


        var notifyIcon = new NotifyIcon(components)
        {
            ContextMenuStrip = contextMenu,
            Icon = Icon,
            Text = commandDescriptor.Title,
            Visible = true
        };

        Controls.Add(layout);
        contextMenu.ResumeLayout(false);
        layout.ResumeLayout(false);
        ResumeLayout(false);

        _command = new Command(commandDescriptor);

        _logBox.Resize += (_, _) =>
        {
            if (WindowState != FormWindowState.Minimized)
                lvColumnMessage.Width = _logBox.Width - lvColumnType.Width - 4;
        };

        notifyIcon.DoubleClick += (_, _) =>
        {
            if (Visible)
                HideToTray();
            else
                RestoreFromTray();
        };

        FormClosing += (sender, e) =>
        {
            Debug.WriteLine("Trying to close");

            if (!_command.Halted && e.CloseReason == CloseReason.UserClosing && !_doClose)
            {
                Debug.WriteLine("Not Closing...");
                e.Cancel = true;
                Hide();
            }
            else
            {
                Debug.WriteLine("Closing...");
                Stop();
            }
        };

        FormClosed += (sender, _) => { Debug.WriteLine("Closed"); };
        Resize += (sender, _) =>
        {
            if (WindowState == FormWindowState.Minimized) HideToTray();
        };

        stopButton.Click += (sender, args) => Stop();
        contextMenu.ItemClicked += (sender, e) =>
        {
            if (e.ClickedItem != _notifyContextMenuExitItem) return;
            _doClose = true;
            Close();
        };

        _command.OutputReceived += (sender, output) => { LogText(output, Source.Out); };

        _command.ErrorReceived += (sender, output) => { LogText(output, Source.Error); };

        _command.Terminated += (_, _) => { _logBox.AppendText("[Terminated]" + Environment.NewLine); };

        Start();
    }

    private void Start()
    {
        try
        {
            _command.Launch();
        }
        catch (Exception ex)
        {
            MessageBox.Show(
                $"Error ejecutando el comando:\n\n{_command.CommandDescriptor}.\n\n{ex.Message}", "TrayRunner Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            Close();
        }
    }

    private void LogText(string output, Source source)
    {
        //BeginEdit();
        var lvItem = new ListViewItem
        {
            Text = source.ToString(),
            SubItems =
            {
                new ListViewItem.ListViewSubItem
                    { Text = output, ForeColor = source == Source.Error ? Color.DarkRed : Color.Empty }
            }
        };

        _logBox.AppendText($"{source}: {output}" + Environment.NewLine);

        //EndEdit();
        // if (_logBox.Items.Count >= 1000)
        // {
        //     _logBox.Items.RemoveAt(0);
        // }

        // _logBox.EnsureVisible(_logBox.Items.Count - 1);
    }

    private void HideToTray()
    {
        Hide();
    }

    private void RestoreFromTray()
    {
        Show();
        WindowState = FormWindowState.Normal;
        Activate();
    }

    private void Stop()
    {
        if (!_command.Halted) _command.Halt();
    }


    /// <summary>
    ///     Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing) components.Dispose();
        base.Dispose(disposing);
    }

    private enum Source
    {
        Out,
        Error
    }
}