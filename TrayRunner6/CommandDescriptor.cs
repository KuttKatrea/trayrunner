namespace net.katrea.trayrunner.tr6;

public class CommandDescriptor
{
    internal string? Title { get; init; }
    internal string? Icon { get; init; }
    internal string? WorkingDirectory { get; init; }
    internal string Command { get; init; } = "";
    internal string[] CommandArguments { get; init; } = Array.Empty<string>();
}