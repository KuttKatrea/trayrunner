using CommandLine;

namespace net.katrea.trayrunner.tr6;

public class Options
{
    [Option('t', "title", Required = false)]
    public string Title { get; set; } = "TrayRunner";

    [Option('i', "icon", Required = false)]
    public string? Icon { get; set; } = null;

    [Option('d', "workdir", Required = false)]
    public string? WorkingDirectory { get; set; } = null;

    [Option('s', "shell", Required = false)]
    public bool Shell { get; set; } = false;

    [Value(0, Required = true, MetaName = "Command")]
    public IEnumerable<string>? Command { get; set; }
}