using System.Diagnostics;
using System.Text;
using CommandLine;

namespace net.katrea.trayrunner.tr6;

internal static class Program
{
    /// <summary>
    ///     The main entry point for the application.
    /// </summary>
    [STAThread]
    private static void Main(string[] args)
    {
        var helpTextBuilder = new StringBuilder();
        var helpTextWriter = new StringWriter(helpTextBuilder);

        var parser = new Parser(settings =>
        {
            // https://stackoverflow.com/a/56920813/2631231
            settings.HelpWriter = helpTextWriter;
            settings.EnableDashDash = true;
        });
        ParserResult<Options> parserResult;

        parserResult = parser.ParseArguments<Options>(args);

        parserResult.WithParsed(o =>
        {
            Debug.Write("Options");
            Debug.Write(o);
            var commandDescriptor = CommandFactory.CreateCommand(o);
            ApplicationConfiguration.Initialize();
            Application.Run(new AppForm(commandDescriptor));
        });

        parserResult.WithNotParsed(_ =>
        {
            MessageBox.Show(helpTextBuilder.ToString(), "TrayRunner", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        });
    }
}